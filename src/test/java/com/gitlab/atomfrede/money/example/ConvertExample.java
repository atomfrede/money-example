package com.gitlab.atomfrede.money.example;

import org.javamoney.moneta.Money;
import org.junit.Test;

import javax.money.convert.ExchangeRate;
import javax.money.convert.ExchangeRateProvider;
import javax.money.convert.MonetaryConversions;
import java.math.BigDecimal;

public class ConvertExample {

    @Test
    public void euroToUsDollar() {

        Money eur = Money.of(new BigDecimal("1.40"), "EUR");

        Money usd = eur.with(MonetaryConversions.getConversion("USD"));

        System.out.println(usd);


    }
}
